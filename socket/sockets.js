import { Server } from "socket.io";
import { v4 as uuid } from "uuid";


let colors = [];
let IdSockets=[]
let NumP=0;
let adminID=null

export default (io) => {
  io.on("connection", (socket) => {
   
    console.log("nuevo socket connectado:", socket.id);
    io.to(adminID).emit("adminn", colors);


    socket.on("cliente:datos", (list) => {  

      const NumRecibido=list.NP-1
      const datos=({...list })
      colors = colors.filter(datos => datos.NP !== list.NP)
      colors.push(datos)

      io.to(adminID).emit("adminn", colors);


      cargarcolores(list.NP);
     // io.to(IdSockets[NumRecibido].Id).emit('mostrar', list);

    });



    socket.on('IdPantallas', datos =>{
      NumP++;
      /*const dato={...datos, Numero: NumP}
      IdSockets.push(dato)*/
      IdSockets[datos.pantalla]=datos.Id
      console.log(IdSockets[2])
        cargarcolores(datos.pantalla);
    })



    function cargarcolores(idpantalla){
      colors.forEach(date => {
          if(date.NP===idpantalla){
            console.log(date)
            io.to(IdSockets[idpantalla]).emit('mostrar', date);
          }
      }) 
    }




    socket.on('admin',Aid=>{
        adminID=Aid
        io.to(Aid).emit("adminn", colors);

    })

    socket.on("disconnect", () => {

      console.log('antes',IdSockets)

      IdSockets = IdSockets.filter(datos => datos !== socket.id)
      console.log(IdSockets)

      console.log(socket.id, "Usuario desconectado");
    });



  });
};
