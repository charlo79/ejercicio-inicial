import express from "express";
import bodyParser from "body-parser";

import { Server as WebSocketServer } from "socket.io";
import http from "http";

import Sockets from "./sockets.js";

import screensRoutes from "./routes/screens.js";

const app = express();
const PORT = 3000;
app.get("/", (req, res) => res.send("SISTEMA DE PANTALLA"));
//app.use("/screens", screensRoutes);

app.use(bodyParser.json());


const server = http.createServer(app);

const httpServer = server.listen(PORT);

const io = new WebSocketServer(httpServer, {
    cors: {
      origin: "*",
      methods: ["GET", "POST"]
    }
});

Sockets(io);

console.log(`Servidor http://localhost:${PORT}`);

