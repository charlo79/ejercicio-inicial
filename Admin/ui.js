
const notesList= document.querySelector('#notes');

const noteUI = note =>{

    const div = document.createElement("div");
    var idglobal;

    div.innerHTML = `
    <div class="card card-body rounded-0 mb-2">
      <div class="d-flex justify-content-between">
         <h1 class="h3 card-title">Pantalla ${note.NP}</h1>
         <div>
         <button class="btn btn-danger delete" data-id="${note.NP}" onclick="deleteP">Eliminar</button>
         <button class="btn btn-secondary Ver" data-id="${note.NP}">Ver</button>
           
         </div>
       </div>
       <p>${note.uno}</p>
       <p>${note.dos}</p>
       <p>${note.tres}</p>
    </div>
   `;

   const btneliminar = div.querySelector(".delete");
   btneliminar.addEventListener('click',() =>{
    idglobal=btneliminar.dataset.id;  
    
    eliminarpantalla(btneliminar.dataset.id)
   })


   const btnver= div.querySelector(".Ver")
   btnver.addEventListener('click',() =>{
        window.open(`http://localhost:5501?pantalla=${btnver.dataset.id}`)   

    })

   return div;

}

const eliminarpantalla =id =>{
    
}

const renderNotes = notes =>{
    notesList.innerHTML="";
    notes.forEach(note => {
        notesList.append(noteUI(note))
    })
}

const appenNote = note =>{
    notesList.append(noteUI(note))    
}
